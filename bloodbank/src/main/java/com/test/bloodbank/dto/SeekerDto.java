package com.test.bloodbank.dto;

public class SeekerDto {
	private String patientName;
	private String bloodGroup;
	private Long bloodUnit;
	private String bloodBankName;
	private String seekCause;
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public String getBloodGroup() {
		return bloodGroup;
	}
	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}
	public Long getBloodUnit() {
		return bloodUnit;
	}
	public void setBloodUnit(Long bloodUnit) {
		this.bloodUnit = bloodUnit;
	}
	public String getBloodBankName() {
		return bloodBankName;
	}
	public void setBloodBankName(String bloodBankName) {
		this.bloodBankName = bloodBankName;
	}
	public String getSeekCause() {
		return seekCause;
	}
	public void setSeekCause(String seekCause) {
		this.seekCause = seekCause;
	}
	
}
