package com.test.bloodbank.dto;

public class AddBloodBankDto {
	private String bloodBankName;
	private Long contact;
	private String email;
	private String state;
	private String district;
	private String address;
	private Long pincode;
	private Long a_Plus=0L;
	private Long o_Plus=0L;
	private Long b_Plus=0L;
	private Long ab_Plus=0L;
	private Long a_minus=0L;
	private Long o_minus=0L;
	private Long b_minus=0L;
	private Long ab_minus=0L;
	public String getBloodBankName() {
		return bloodBankName;
	}
	public void setBloodBankName(String bloodBankName) {
		this.bloodBankName = bloodBankName;
	}
	public Long getContact() {
		return contact;
	}
	public void setContact(Long contact) {
		this.contact = contact;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Long getPincode() {
		return pincode;
	}
	public void setPincode(Long pincode) {
		this.pincode = pincode;
	}
	public Long getA_Plus() {
		return a_Plus;
	}
	public void setA_Plus(Long a_Plus) {
		this.a_Plus = a_Plus;
	}
	public Long getO_Plus() {
		return o_Plus;
	}
	public void setO_Plus(Long o_Plus) {
		this.o_Plus = o_Plus;
	}
	public Long getB_Plus() {
		return b_Plus;
	}
	public void setB_Plus(Long b_Plus) {
		this.b_Plus = b_Plus;
	}
	public Long getAb_Plus() {
		return ab_Plus;
	}
	public void setAb_Plus(Long ab_Plus) {
		this.ab_Plus = ab_Plus;
	}
	public Long getA_minus() {
		return a_minus;
	}
	public void setA_minus(Long a_minus) {
		this.a_minus = a_minus;
	}
	public Long getO_minus() {
		return o_minus;
	}
	public void setO_minus(Long o_minus) {
		this.o_minus = o_minus;
	}
	public Long getB_minus() {
		return b_minus;
	}
	public void setB_minus(Long b_minus) {
		this.b_minus = b_minus;
	}
	public Long getAb_minus() {
		return ab_minus;
	}
	public void setAb_minus(Long ab_minus) {
		this.ab_minus = ab_minus;
	}
	
	
}
