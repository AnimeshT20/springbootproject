package com.test.bloodbank.dto;

public class UserDto {
	    private String firstName;
	    private String lastName;
	    private String username;
	    private String password;
	    private String rePassword;
	    private String gender;
	    private String dob;
	    private Long contact;
	    private String email;
	    private Long aadhaarNumber;
	    private String bloodGroup;
	    private String state;
	    private String district;
		public String getFirstName() {
			return firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		public String getLastName() {
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public String getGender() {
			return gender;
		}
		public void setGender(String gender) {
			this.gender = gender;
		}
		public String getDob() {
			return dob;
		}
		public void setDob(String dob) {
			this.dob = dob;
		}
		public Long getContact() {
			return contact;
		}
		public void setContact(Long contact) {
			this.contact = contact;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public Long getAadhaarNumber() {
			return aadhaarNumber;
		}
		public void setAadhaarNumber(Long aadhaarNumber) {
			this.aadhaarNumber = aadhaarNumber;
		}
		public String getBloodGroup() {
			return bloodGroup;
		}
		public void setBloodGroup(String bloodGroup) {
			this.bloodGroup = bloodGroup;
		}
		public String getState() {
			return state;
		}
		public void setState(String state) {
			this.state = state;
		}
		public String getDistrict() {
			return district;
		}
		public void setDistrict(String district) {
			this.district = district;
		}
		public String getRePassword() {
			return rePassword;
		}
		public void setRePassword(String rePassword) {
			this.rePassword = rePassword;
		}
	    
	    
}
