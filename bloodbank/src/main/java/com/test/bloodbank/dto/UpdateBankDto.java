package com.test.bloodbank.dto;

public class UpdateBankDto {
	private Long contact;
	private String email;
	private Long aPlus;
	private Long oPlus;
	private Long bPlus;
	private Long abPlus;
	private Long aMinus;
	private Long bMinus;
	private Long oMinus;
	private Long abMinus;
	public Long getContact() {
		return contact;
	}
	public void setContact(Long contact) {
		this.contact = contact;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Long getaPlus() {
		return aPlus;
	}
	public void setaPlus(Long aPlus) {
		this.aPlus = aPlus;
	}
	public Long getoPlus() {
		return oPlus;
	}
	public void setoPlus(Long oPlus) {
		this.oPlus = oPlus;
	}
	public Long getbPlus() {
		return bPlus;
	}
	public void setbPlus(Long bPlus) {
		this.bPlus = bPlus;
	}
	public Long getAbPlus() {
		return abPlus;
	}
	public void setAbPlus(Long abPlus) {
		this.abPlus = abPlus;
	}
	public Long getaMinus() {
		return aMinus;
	}
	public void setaMinus(Long aMinus) {
		this.aMinus = aMinus;
	}
	public Long getbMinus() {
		return bMinus;
	}
	public void setbMinus(Long bMinus) {
		this.bMinus = bMinus;
	}
	public Long getoMinus() {
		return oMinus;
	}
	public void setoMinus(Long oMinus) {
		this.oMinus = oMinus;
	}
	public Long getAbMinus() {
		return abMinus;
	}
	public void setAbMinus(Long abMinus) {
		this.abMinus = abMinus;
	}
	
}
