package com.test.bloodbank.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.test.bloodbank.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{
	
	User findByUsernameAndPassword(String username, String passsword);

	User findByUsername(String username);
	
	User findByUserId(Long userId);
	
	List<User> findByRole(String role);
}
