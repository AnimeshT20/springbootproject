package com.test.bloodbank.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.test.bloodbank.model.BloodCamps;

@Repository
public interface BloodCampsRepository extends JpaRepository<BloodCamps, Long> {
	BloodCamps findByCampId(Long campId);
}
