package com.test.bloodbank.serviceImpl;

import java.sql.Date;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.test.bloodbank.dto.DonorDto;
import com.test.bloodbank.dto.ManageAppointmentDto;
import com.test.bloodbank.exceptions.EarlyDonationException;
import com.test.bloodbank.exceptions.InvalidAgeException;
import com.test.bloodbank.exceptions.InvalidWeightException;
import com.test.bloodbank.model.Appointment;
import com.test.bloodbank.model.BloodBank;
import com.test.bloodbank.model.Transaction;
import com.test.bloodbank.model.User;
import com.test.bloodbank.repository.AppointmentRepository;
import com.test.bloodbank.repository.BloodBankRepository;
import com.test.bloodbank.repository.TransactionRepository;
import com.test.bloodbank.repository.UserRepository;
import com.test.bloodbank.service.AppointmentService;
import com.test.bloodbank.util.UtilConstants;

@Service
@Transactional
public class AppointmentServiceImpl implements AppointmentService {
	
    private static final Logger LOGGER = LoggerFactory.getLogger(AppointmentService.class);
    
    @Autowired
    private AppointmentRepository appRepository;
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private BloodBankRepository bankRepository;
    
    @Autowired
    private TransactionRepository transRepository;

	public static Logger getLogger() {
		return LOGGER;
	}
	
	@Override
	public Appointment manageAppointment(Long appointmentId, ManageAppointmentDto manageDto) {
		DateFormat outputFormatter = new SimpleDateFormat("MM/dd/yyyy");
        Date output = Date.valueOf(outputFormatter.format(manageDto.getDate())); 
		Appointment appointment = new Appointment();
		BloodBank bloodBank = new BloodBank();
		bloodBank = bankRepository.findByBloodBankName(manageDto.getBloodBankName());
		appointment = appRepository.findByAppointmentId(appointmentId);
		appointment.setBloodBank(bloodBank);
		appointment.setDate(output);
		appointment.setTimeStamp(Time.valueOf(manageDto.getTimestamp()));	
		appointment.setStatus(UtilConstants.BLOOD_DONATE_APPLIED);
		return appRepository.saveAndFlush(appointment);
	}
	
	@Override
	public Appointment scheduleAppointment(String username, DonorDto donorDto) throws InvalidWeightException, InvalidAgeException, EarlyDonationException {
		java.util.Date current=Calendar.getInstance().getTime();

		if(donorDto.getAge()<18) {throw new InvalidAgeException("");}
		if(donorDto.getWeight()<50) {throw new InvalidWeightException("");}
		if(TimeUnit.MILLISECONDS.toDays(current.getTime()- Date.valueOf(donorDto.getLastDonationDate()).getTime())<90)
		{throw new EarlyDonationException("");}
		Appointment appointment = new Appointment();
		DateFormat outputFormatter = new SimpleDateFormat("yyyy/MM/dd");
        java.util.Date output = Date.valueOf(outputFormatter.format(donorDto.getDate())); 
		BloodBank bloodBank = bankRepository.findByBloodBankName(donorDto.getBloodBankName());
		User user = userRepository.findByUsername(username);
		appointment.setBloodBank(bloodBank);
		appointment.setUser(user);
		appointment.setDate(Date.valueOf(donorDto.getDate()));
		appointment.setTimeStamp(Time.valueOf(donorDto.getTime()));
		appointment.setStatus(UtilConstants.BLOOD_DONATE_APPLIED);
		
		return appRepository.saveAndFlush(appointment);
	}
	
	@Override
	public Appointment approveAppointment(Long appointmentId) {
		Appointment appointment = new Appointment();
		appointment = appRepository.getReferenceById(appointmentId);
		appointment.setStatus(UtilConstants.BLOOD_DONATE_APPROVED);
		
		Transaction t = new Transaction();
		t.setBloodBank(appointment.getBloodBank());
		t.setBloodGroup(appointment.getUser().getBloodGroup());
		t.setUser(appointment.getUser());
		t.setCause("~~");
		t.setPatient("~~");
		t.setUnitOfBloodGroup(1L);
		t.setStatus(UtilConstants.DONATION_STATUS_PENDING);
		t.setTimeStamp(appointment.getTimeStamp());
		transRepository.save(t);
		
		return appRepository.saveAndFlush(appointment);
	}
	
	@Override
	public Appointment deniedAppointment(Long appointmentId) {
		Appointment appointment = new Appointment();
		appointment = appRepository.getReferenceById(appointmentId);
		appointment.setStatus(UtilConstants.REQUEST_DENIED);
		
		Transaction t = new Transaction();
		t.setBloodBank(appointment.getBloodBank());
		t.setBloodGroup(appointment.getUser().getBloodGroup());
		t.setUser(appointment.getUser());
		t.setCause("~~");
		t.setPatient("~~");
		t.setUnitOfBloodGroup(1L);
		t.setStatus(UtilConstants.DONATION_STATUS_FAILED);
		t.setTimeStamp(appointment.getTimeStamp());
		transRepository.save(t);
		
		return appRepository.saveAndFlush(appointment);
	}
	
	@Override
	public List<Appointment> getAllAppointments(){
		return appRepository.findAll();
	}
	
	@Override
	public List<Appointment> showRequests(){
		return appRepository.findByStatus(UtilConstants.BLOOD_DONATE_APPLIED);
	}
	
	@Override
	public List<Appointment> getAppointment(User user){
		return appRepository.findByUser(user);
	}
	
	@Override
	public void cancelAppointment(Long appointmentId){
		appRepository.deleteById(appointmentId);
	}
	 
}
