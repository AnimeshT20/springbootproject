package com.test.bloodbank.serviceImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.bloodbank.dto.AddBloodBankDto;
import com.test.bloodbank.dto.BankSearchDto;
import com.test.bloodbank.dto.BloodSearchDto;
import com.test.bloodbank.dto.UpdateBankDto;
import com.test.bloodbank.model.BloodBank;
import com.test.bloodbank.repository.BloodBankRepository;
import com.test.bloodbank.service.BloodBankService;

@Service
@Transactional
public class BloodBankServiceImpl  implements BloodBankService{
	 
	private static final Logger LOGGER = LoggerFactory.getLogger(BloodBankServiceImpl.class);
	
	@Autowired
	private BloodBankRepository bankRepository;

	public static Logger getLogger() {
		return LOGGER;
	}
	
	@Override
	public BloodBank addBloodBank(AddBloodBankDto bloodBankdto) {
		BloodBank bloodBank = new BloodBank(bloodBankdto.getBloodBankName(), bloodBankdto.getContact(),
				bloodBankdto.getEmail(),bloodBankdto.getState(), bloodBankdto.getDistrict(),bloodBankdto.getAddress(), 
				bloodBankdto.getPincode(), bloodBankdto.getA_Plus(), bloodBankdto.getB_Plus(), bloodBankdto.getO_Plus(), 
				bloodBankdto.getAb_Plus(), bloodBankdto.getA_minus(), bloodBankdto.getO_minus(), bloodBankdto.getB_minus(), 
				bloodBankdto.getAb_minus());
		
		return bankRepository.saveAndFlush(bloodBank);
	}
	
	@Override
	public List<BloodBank> viewAllBloodBanks(){
		return bankRepository.findAll();
	}
	
	@Override
	public List<BloodBank> viewBloodBanksByLocation(BankSearchDto bankDto){
		return bankRepository.findByDistrictAndState(bankDto.getDistrict(), bankDto.getState());
	}
	
	@Override
	public BloodBank viewBloodBankByName(String bankName){
		return bankRepository.findByBloodBankName(bankName);
	}
	
	@Override
	public void deleteBloodBank(Long bloodBankId) {
		bankRepository.deleteById(bloodBankId);
	}
	
	@Override
	public BloodBank updateBloodBank(Long bloodBankId, UpdateBankDto updateDto) {
		BloodBank bloodBank = bankRepository.findByBloodBankId(bloodBankId);
		bloodBank.setContact(updateDto.getContact());
		bloodBank.setEmail(updateDto.getEmail());
		bloodBank.setaPlus(bloodBank.getaPlus()+updateDto.getaPlus());
		bloodBank.setbPlus(bloodBank.getbPlus()+updateDto.getbPlus());
		bloodBank.setoPlus(bloodBank.getoPlus()+updateDto.getoPlus());
		bloodBank.setAbPlus(bloodBank.getAbPlus()+updateDto.getAbPlus());
		bloodBank.setaMinus(bloodBank.getaMinus()+updateDto.getaMinus());
		bloodBank.setbMinus(bloodBank.getbMinus()+updateDto.getbMinus());
		bloodBank.setoMinus(bloodBank.getoMinus()+updateDto.getoMinus());
		bloodBank.setAbMinus(bloodBank.getAbMinus()+updateDto.getAbMinus());
		
		return bankRepository.saveAndFlush(bloodBank);
	}
	
	@Override
	public List<BloodBank> getBankByBloodType(BloodSearchDto bloodDto){
		String bloodType = bloodDto.getBloodGroup();
		String state = bloodDto.getState();
		String district = bloodDto.getDistrict();
		if(bloodType.equalsIgnoreCase("a_plus")) {
			return bankRepository.findBanksByAPlus(state, district);
		}
		if(bloodType.equalsIgnoreCase("a_minus")) {
			return bankRepository.findBanksByAMinus(state, district);
		}
		if(bloodType.equalsIgnoreCase("b_plus")) {
			return bankRepository.findBanksByBPlus(state, district);
		}
		if(bloodType.equalsIgnoreCase("b_minus")) {
			return bankRepository.findBanksByBMinus(state, district);
		}
		if(bloodType.equalsIgnoreCase("o_plus")) {
			return bankRepository.findBanksByOPlus(state, district);
		}
		if(bloodType.equalsIgnoreCase("o_minus")) {
			return bankRepository.findBanksByOMinus(state, district);
		}
		if(bloodType.equalsIgnoreCase("ab_plus")) {
			return bankRepository.findBanksByABPlus(state, district);
		}
		if(bloodType.equalsIgnoreCase("ab_minus")) {
			return bankRepository.findBanksByABMinus(state, district);
		}	

		return null;
	}

	
}
