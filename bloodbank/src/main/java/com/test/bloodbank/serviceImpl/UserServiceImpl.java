package com.test.bloodbank.serviceImpl;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import org.springframework.security.crypto.password.PasswordEncoder;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.test.bloodbank.dto.UpdateUserDto;
import com.test.bloodbank.dto.UserDto;
import com.test.bloodbank.exceptions.InvalidPasswordException;
import com.test.bloodbank.exceptions.UserExistException;
import com.test.bloodbank.model.User;
import com.test.bloodbank.repository.UserRepository;
import com.test.bloodbank.service.UserService;
import com.test.bloodbank.util.UtilConstants;

@Service
@Transactional
public class UserServiceImpl implements UserService{
	
    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);
	
	@Autowired 
	private UserRepository userRepository;
	
    @Autowired
    private PasswordEncoder passwordEncoder;
	
	public static Logger getLogger() {
		return LOGGER;
	}

	//Encoding the password
    public String getEncodePassword(String password){
        return passwordEncoder.encode(password);
    }
	
    @Override
	public User registerUser(UserDto userDto) throws UserExistException, InvalidPasswordException{
		if(!userDto.getPassword().equals(userDto.getRePassword()))
			throw new InvalidPasswordException("");
        User existingUserResponse = userRepository.findByUsername(userDto.getUsername());
        if(existingUserResponse != null){
            LOGGER.info("" + userDto.getUsername() + " : This username is already registered ");
			throw new UserExistException("");
        }
        DateFormat outputFormatter = new SimpleDateFormat("MM/dd/yyyy");
        Date output = Date.valueOf(outputFormatter.format(userDto.getDob())); 
        User user = new User();
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setAadhaarNumber(userDto.getAadhaarNumber());
        user.setBloodGroup(userDto.getBloodGroup());
        user.setContact(userDto.getContact());
        user.setDistrict(userDto.getDistrict());
        user.setState(userDto.getState());
        user.setDob(output);
        user.setEmail(userDto.getEmail());
        user.setGender(userDto.getGender());
        user.setUsername(userDto.getUsername());
        user.setPassword(getEncodePassword(userDto.getPassword()));
        user.setRole(UtilConstants.USER_ROLE_CLIENT);

        LOGGER.info("Saving the New User details into Database");
        return userRepository.saveAndFlush(user);

    }
	
    @Override
	public List<User> showAllUsers(){
		return userRepository.findByRole(UtilConstants.USER_ROLE_CLIENT);
	}
	
    @Override
	public User showUserdetails(String username) {
		return userRepository.findByUsername(username);
	}
	
    @Override
	public User updateUser(String username, UpdateUserDto updateDto ){
        User existingUser = userRepository.findByUsername(username);
        existingUser.setContact(updateDto.getContact());
        existingUser.setEmail(updateDto.getEmail());
        existingUser.setState(updateDto.getState());
        existingUser.setDistrict(updateDto.getDistrict());
       
        return userRepository.saveAndFlush(existingUser);

	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		User user = userRepository.findByUsername(username);
		if(user == null) {
			throw new UsernameNotFoundException("Invalid username or password.");
		}
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), mapRolesToAuthorities(user.getRole()));		
	
	}
	
	private Collection<? extends GrantedAuthority> mapRolesToAuthorities(String role){
		SimpleGrantedAuthority simpleGrantedAuthority=new SimpleGrantedAuthority(role);
		return List.of(simpleGrantedAuthority);
	}
	

	
	
	
	

	
	
	
	
	
}
