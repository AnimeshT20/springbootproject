package com.test.bloodbank.serviceImpl;

import java.util.Calendar;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.bloodbank.dto.SeekerDto;
import com.test.bloodbank.model.BloodBank;
import com.test.bloodbank.model.Transaction;
import com.test.bloodbank.model.User;
import com.test.bloodbank.repository.BloodBankRepository;
import com.test.bloodbank.repository.TransactionRepository;
import com.test.bloodbank.repository.UserRepository;
import com.test.bloodbank.service.TransactionService;
import com.test.bloodbank.util.UtilConstants;

@Service
@Transactional
public class TransactionServiceImpl implements TransactionService{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TransactionServiceImpl.class);
	
	@Autowired 
	private TransactionRepository transRepository;

	@Autowired 
	private UserRepository userRepository;
	
	@Autowired 
	private BloodBankRepository bankRepository;
	
	public static Logger getLogger() {
		return LOGGER;
	}
	
	@Override
	public Transaction insertTransaction(Transaction transaction) {
		return transRepository.saveAndFlush(transaction);
	}
	
	@Override
	public List<Transaction> viewAllTransaction(){
		return transRepository.findAll();
	}
	
	@Override
	public List<Transaction> viewAllTransactionByUser(String username){
		User user = userRepository.findByUsername(username);
		return transRepository.findByUser(user);
	}
	
	@Override
	public Transaction seekBlood(String username, SeekerDto seekDto) {
		User user = userRepository.findByUsername(username);
        BloodBank bloodBank = bankRepository.findByBloodBankName(seekDto.getBloodBankName());
        Transaction t = new Transaction();
        t.setBloodBank(bloodBank);
        t.setUser(user);
        t.setBloodGroup(seekDto.getBloodGroup());
        t.setCause(seekDto.getSeekCause());
        t.setPatient(seekDto.getPatientName());
        t.setUnitOfBloodGroup(seekDto.getBloodUnit());
        t.setTimeStamp(Calendar.getInstance().getTime());
        t.setStatus(UtilConstants.BLOOD_SEEK_APPLIED);
        return transRepository.saveAndFlush(t);
	}
	
	@Override
	public Transaction approveSeekRequest(Long transId) {
		Transaction t = transRepository.getReferenceById(transId);
		t.setStatus(UtilConstants.BLOOD_SEEK_APPROVED);

		return transRepository.saveAndFlush(t);
		
	}	
	
	@Override
	public Transaction declineSeekRequest(Long transId) {
		Transaction t = transRepository.getReferenceById(transId);
		t.setStatus(UtilConstants.REQUEST_DENIED);

		return transRepository.saveAndFlush(t);
		
	}

	@Override
	public List<Transaction> showRequests(){
		return transRepository.findByStatus(UtilConstants.BLOOD_DONATE_APPLIED);
	}

}
