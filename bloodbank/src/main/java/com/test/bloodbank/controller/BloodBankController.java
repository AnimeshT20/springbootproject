package com.test.bloodbank.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.test.bloodbank.dto.AddBloodBankDto;
import com.test.bloodbank.dto.BankSearchDto;
import com.test.bloodbank.dto.BloodSearchDto;
import com.test.bloodbank.model.BloodBank;
import com.test.bloodbank.serviceImpl.BloodBankServiceImpl;

@Controller
public class BloodBankController {
	
	@Autowired
	private BloodBankServiceImpl bankService;
	
	@GetMapping("/admin/addbank")
	public String newBankDetails(Model model) {
		AddBloodBankDto bank = new AddBloodBankDto();
		model.addAttribute("bank",bank);
		return "addbank";
	}
	@PostMapping("/admin/addBank")
	public String addBank(@ModelAttribute("bank") AddBloodBankDto bankDto){
		BloodBank bank = bankService.viewBloodBankByName(bankDto.getBloodBankName());
		System.out.println(bank==null); System.out.println("--------------------------------------------------------------------------------------");
		if(bank!=null) {
			return "redirect:/admin/addbank?name";
		}
		bankService.addBloodBank(bankDto);
		return "redirect:/?success";
	}
	@GetMapping("/allBanks")
	public String showAllBanks(Model model) {
		BankSearchDto banksearchDto = new BankSearchDto();
		model.addAttribute("banksearch", banksearchDto);
		model.addAttribute("banks", bankService.viewAllBloodBanks());
		return "viewbanks";
	}
	
	@GetMapping("/allBanks2")
	public String showAllBanksByLoc(@ModelAttribute("banklist")List<BloodBank> banks) {
		
		return "viewbanks";
	}
	
	@PostMapping("/allBanks")
	public String showBanksByLoc(@ModelAttribute("banksearch") BankSearchDto bankDto, Model model){
		List<BloodBank> banks = bankService.viewBloodBanksByLocation(bankDto);
		model.addAttribute("banklist",banks);
		if(banks==null) {
			return "redirect:/allBanks?nobanks";
		}
		return "redirect:/allBanks2";
	}
	
	@GetMapping("/bloodsearch")
	public String bloodDetails(Model model) {
		BloodSearchDto bloodDto = new BloodSearchDto();
		model.addAttribute("blood", bloodDto);
		return "bloodsearch";
	}
	@PostMapping("/bloodsearch")
	public String showBanksByBlood(@ModelAttribute("blood") BloodSearchDto bloodDto, Model model){
		List<BloodBank> banks = bankService.getBankByBloodType(bloodDto);
		model.addAttribute("banklist",banks);
		if(banks==null) {
			return "redirect/bloodsearch?fail";
		}
		return "redirect:/bloodsearch?success";
	}
	
}
