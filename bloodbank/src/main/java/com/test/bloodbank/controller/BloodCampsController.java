package com.test.bloodbank.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.test.bloodbank.dto.AddCampDto;
import com.test.bloodbank.service.BloodBankService;
import com.test.bloodbank.service.BloodCampsService;


@Controller
public class BloodCampsController {
	
	@Autowired
	private BloodCampsService campService;
	
	@Autowired
	private BloodBankService bankService;
	
	@GetMapping("/allCamps")
	public String showAllCamps(Model model) {
		model.addAttribute("camps", campService.listAllCamps());
		return "viewcamps";
	}
	@GetMapping("/organiseCamp")
	public String newCampDetails(Model model) {
		AddCampDto campDto = new AddCampDto();
		model.addAttribute("camp", campDto);
		model.addAttribute("banks", bankService.viewAllBloodBanks() );
		return "organisecamps";
	}
	@PostMapping("/organiseCamp")
	public String organiseCamp(@ModelAttribute("camp") AddCampDto campDto){
		if(campService.addCamp(campDto)!=null)
			return "redirect:/allcamps?success";
		return "redirect:/organiseCamp?error";	
	}
}
