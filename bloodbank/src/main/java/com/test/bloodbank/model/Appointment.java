package com.test.bloodbank.model;

import java.util.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity 
@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class Appointment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long appointmentId;
	private Date date;
	@DateTimeFormat(pattern="hh-mm-ss")
	private Date timeStamp;
	private String status; 
	@OneToOne
	@JoinColumn(name = "userId", referencedColumnName = "userId" )
	private User user; 
	@OneToOne
	@JoinColumn(name = "bloodBankId", referencedColumnName = "bloodBankId" )
	private BloodBank bloodBank;
	
	public Appointment() {}
	
	public Appointment(Long appointmentId, Date date, Date timeStamp, User user, BloodBank bloodBank, String status) {
		this.appointmentId = appointmentId;
		this.date = date;
		this.timeStamp = timeStamp;
		this.user = user;
		this.bloodBank = bloodBank;
		this.status = status;
	}

	public Long getId() {
		return appointmentId;
	}

	public void setId(Long id) {
		this.appointmentId = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	public BloodBank getBloodBank() {
		return bloodBank;
	}

	public void setBloodBank(BloodBank bloodBank) {
		this.bloodBank = bloodBank;
	}

	public Long getAppointmentId() {
		return appointmentId;
	}

	public void setAppointmentId(Long appointmentId) {
		this.appointmentId = appointmentId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
}
