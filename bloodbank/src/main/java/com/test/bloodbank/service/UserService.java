package com.test.bloodbank.service;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.test.bloodbank.dto.UpdateUserDto;
import com.test.bloodbank.dto.UserDto;
import com.test.bloodbank.exceptions.InvalidPasswordException;
import com.test.bloodbank.exceptions.UserExistException;
import com.test.bloodbank.model.User;

public interface UserService extends UserDetailsService {
	
	User registerUser(UserDto userDto) throws UserExistException, InvalidPasswordException;
	
	List<User> showAllUsers();
	
	User showUserdetails(String username);
	
	User updateUser(String username, UpdateUserDto updateDto);
}
