package com.test.bloodbank.service;

import java.util.List;

import com.test.bloodbank.dto.SeekerDto;
import com.test.bloodbank.model.Transaction;

public interface TransactionService {
	Transaction insertTransaction(Transaction transaction);
	
	List<Transaction> viewAllTransaction();
	
	List<Transaction> viewAllTransactionByUser(String username);
	
	Transaction seekBlood(String username, SeekerDto seekDto);

	List<Transaction> showRequests();

	Transaction approveSeekRequest(Long transId);

	Transaction declineSeekRequest(Long transId);
}
