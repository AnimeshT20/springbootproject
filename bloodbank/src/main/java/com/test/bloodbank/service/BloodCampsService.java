package com.test.bloodbank.service;

import java.util.List;

import com.test.bloodbank.dto.AddCampDto;
import com.test.bloodbank.model.BloodCamps;

public interface BloodCampsService{

	BloodCamps addCamp(AddCampDto campDto);
	BloodCamps approveBloodCamp(Long bloodCampId);
	List<BloodCamps> listAllCamps();
	BloodCamps displaySpecificCamp(Long campId);
	
}
