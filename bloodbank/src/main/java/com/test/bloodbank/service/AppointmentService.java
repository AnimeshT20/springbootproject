package com.test.bloodbank.service;

import java.text.ParseException;
import java.util.List;

import com.test.bloodbank.dto.DonorDto;
import com.test.bloodbank.dto.ManageAppointmentDto;
import com.test.bloodbank.exceptions.EarlyDonationException;
import com.test.bloodbank.exceptions.InvalidAgeException;
import com.test.bloodbank.exceptions.InvalidWeightException;
import com.test.bloodbank.model.Appointment;
import com.test.bloodbank.model.User;

public interface AppointmentService{
	
	Appointment manageAppointment(Long appointmentId, ManageAppointmentDto manageDto);
	
	Appointment scheduleAppointment(String username, DonorDto donorDto) throws InvalidWeightException, InvalidAgeException, EarlyDonationException, ParseException;
	
	Appointment approveAppointment(Long appointmentId);
	
	Appointment deniedAppointment(Long appointmentId);
	
	List<Appointment> getAllAppointments();
	
	List<Appointment> getAppointment(User user);
	
	void cancelAppointment(Long appointmentId);

	List<Appointment> showRequests();
	
}
